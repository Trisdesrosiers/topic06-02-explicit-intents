package cs518.samples.intentdata1

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.widget.TextView

/**
 *
 * The sole reason for this class is to receive data from the Intent of the Activity that
 * started it.
 * It displays the data on the UI.
 * @author Tricia
 */
class Activity2 : Activity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity2_main)

        val value = intent.extras!!.getString("DATA1")
        val tv1 = findViewById<View>(R.id.tv1) as TextView
        if (value == null || value.isEmpty()) {
            tv1.setText(R.string.nodata)
            tv1.setTextColor(Color.RED)
        } else {
            MainActivity.logIt("DATA1 $value")
            tv1.text = value
            tv1.setTextColor(Color.MAGENTA)
        }
    } // onCreate()

}
