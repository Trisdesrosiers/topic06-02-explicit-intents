package cs518.samples.intentdata1

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText

class MainActivity : Activity() {
    internal lateinit var et: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        et = findViewById(R.id.data1) as EditText
    }

    fun launchActivity2(view: View) {
        val i = Intent(this, Activity2::class.java)

        logIt(et.text.toString())
        i.putExtra("DATA1", et.text.toString())

        startActivity(i)

    }

    companion object {
        fun logIt(msg: String) {
            val TAG = "INTDATA1"
            Log.d(TAG, msg)
        }
    }
}